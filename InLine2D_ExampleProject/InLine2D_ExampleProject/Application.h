#pragma once
#include "inLine2D.h"
#include <fstream>
using namespace iL2D;

class Application : public inLine2D
{
public:
	 Application();
	 ~Application();
	 void OnInitialize();
	 void OnUpdate();
	 void OnDraw();
	 void OnClose();

private:
	IL_Sprite* background;
};
