#pragma once
#include "inLine2D.h"
using namespace iL2D;

class Game : public inLine2D
{
public:
	//--- engine(Don't change) ---//
	Game::Game(int width, int height, bool fullScreen, char* windowName) : inLine2D(width, height, fullScreen, windowName) {}
	void OnInitialize();
	void OnUpdate();
	void OnDraw();
	void OnClose();
	//--- end ---//

	//---Your public functions here ---//

	//--- end ---//
private:
	//---Your private variables here ---//
	IL_Texture* _particleTexture;
	IL_ParticleSystem* _particleSystem;
	//--- end ---//
};

