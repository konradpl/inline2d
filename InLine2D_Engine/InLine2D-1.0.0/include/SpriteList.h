#pragma once
#include "stdafx.h"
#include "Sprite.h"
#include <vector>

class DLLEXPORT IL_SpriteList
{
public:
	IL_SpriteList();
	~IL_SpriteList();
	void AddSprite(IL_Sprite* newSprite);
	void RemoveSprite(int index);
	void DrawAll();
	void DrawSprite(int index);
	IL_Sprite* GetSprite(int index);
	void ClearSpriteList();
	int GetListSize();

private:
	std::vector< IL_Sprite*> *_spriteList;
};

