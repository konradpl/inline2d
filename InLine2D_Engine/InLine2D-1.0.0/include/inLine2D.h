#pragma once
#include "stdafx.h"
#include "inLine2D_includes.h"

namespace iL2D
{
	class DLLEXPORT inLine2D
	{
	public:
		inLine2D();
		virtual ~inLine2D();
		 
		//Starts InLine2D Engine
		void IL_StartApplication(int width, int height, bool fullScreen, char* windowName);
		//Closes whole application
		void IL_EndApplication(); 

		//public function to override

		//runs once when the game starts
		virtual void OnInitialize() = 0; 
		//used for drawing all the textures to the screen
		virtual void OnDraw() = 0;
		//used to update any game logic
		virtual void OnUpdate() = 0;
		//runs when the game closes, use for cleaning up memory
		virtual void OnClose() = 0;

		//getting screen width in pixels
		int IL_GetScreenWidth() { return _screenInformation.width; }
		//getting screen height in pixels
		int IL_GetScreenHeight() { return _screenInformation.height; }
		//set resolution of the screen
		void IL_SetWindowResolution(int width, int height);
		//getting wether screen is fullscreen or not
		bool IL_IsScreenFullScreen() { return _screenInformation.fullScreen; }
		//set full screen or window mode
		void IL_SetFullScreen(bool toggle);

		//getting screen renderer
		SDL_Renderer* IL_GetRenderer() { return _Renderer; };

		//setting frame rate cap
		void IL_SetFrameRateCap(int FPS);
		//turning off/on frame rate cap
		void IL_ToggleFrameRateCap(bool toggle);
		//getting time it took to render this frame
		float* IL_GetDeltaTime() { return _deltaTime; }

		//create a rectangle
		IL_Rectangle* IL_CreateRectangle(Vector2i position, Vector2i scale, Color color, Color color_border);
		//delete the rectangle created
		void IL_DestroyRectangle(IL_Rectangle* rectangle);
		//creating sprite (eg. IL_Sprite* someSprite = IL_CreateSprite(..))
		IL_Sprite* IL_CreateSprite(Vector2f position, Vector2f scale, char* fileName);
		//creating sprite with default width and height of an image (use for ILSprite*)
		IL_Sprite* IL_CreateSprite(Vector2f position, char*  fileName);
		//creating sprite with already existing texture (+ Scale)
		IL_Sprite* IL_CreateSprite(Vector2f position, Vector2f scale, IL_Texture* texture);
		//creating sprite with already existing texture (no Scale option)
		IL_Sprite* IL_CreateSprite(Vector2f position, IL_Texture* texture);
		//removing sprite from memory 
		void IL_DestroySprite(IL_Sprite* sprite);
		//creating texture which can be passed into sprite, use LoadImage after
		IL_Texture* IL_CreateTexture();
		//creating texture and loading image when created, dont need LoadImage after
		IL_Texture* IL_CreateTexture(char* fileName);
		//destroy created texture
		void IL_DestroyTexture(IL_Texture* texture);
		//Create particle System
		//creates lists of sprites
		IL_SpriteList* IL_CreateSpriteList();
		//destroys lists of sprites
		void IL_DestroySpriteList(IL_SpriteList* spriteList);
		//creating text (eg. IL_Text* someText = IL_CreateText(..))
		IL_Text* IL_CreateText(TTF_Font* font, Color color, Vector2f position, char* contentText = "", unsigned int wrapWidth = 0);
		//removing text information from memory
		void IL_DestroyText(IL_Text* text);
		//create timer
		IL_Timer* IL_CreateTimer(int seconds);
		//remove timer from memory
		void IL_DestroyTimer(IL_Timer* timer);
		IL_Button* IL_CreateButton(char* defaultButtonFilePath, char* pressedButtonFilePath, char* disabledButtonFilePath, TTF_Font* font, Color defaultTextColor, Color pressedTextColor, Color disabledTextColor, Vector2f position, Vector2f size, char* contentText = "");
		void IL_DestroyButton(IL_Button* button);
		//IL_ParticleSystem* IL_CreateParticleSystem(Vector2i position, float particleSize, float particleLifeTime, float particleSpawnRate, int particleAmount, IL_Texture* particleTexture);
		//Remove Particle System From Memory
		//void IL_DestroyParticleSystem(IL_ParticleSystem* particleSystem);

		void IL_TakeScreenShot(const char* savePath);
		//Class responsible for all Audio
		Audio* IL_AudioManager() { return _AudioManager; }
		//Class responsible for all Input
		Input* IL_InputManager() { return _InputManager; }
		//Calss responsible for all Text
		TextManager* IL_TextManager() { return _TextManager; }
		GUIManager* IL_GUIManager() { return _GUIManager; }

	private:
		//storing all information about the Window
		struct ScreenInformation { int height; int width; bool fullScreen; char*  windowName; int windowType; };

		Audio* _AudioManager;
		Input* _InputManager;
		TextManager* _TextManager;
		GUIManager* _GUIManager;
		//Time variables used to calcuate delta time
		Uint32 _oldTime, _currentTime;
		//Time since last frame
		float *_deltaTime;

		//Inline Internal Functions
		bool inLine2D_Initialize();//Initializes Engine
		void inLine2D_Update(); //Updates Engine
		void inLine2D_Close(); //Closes application
		void UpdateTime(); //Updates Time
		
		 //update window resolution after it was changed to make sure it is still correct
		void UpdateScreenResolution();

		//storing infromation about screen
		ScreenInformation _screenInformation;
		//storing the Window and Renderer
		SDL_Window* _Window = NULL;
		SDL_Renderer* _Renderer = NULL;
		//framerate cap
		Uint32 MAX_FPS;
		//used for closing application
		bool _endApplication;
		bool _frameRateCapEnabled;

		//storing SDL event
		SDL_Event e;
	};
}