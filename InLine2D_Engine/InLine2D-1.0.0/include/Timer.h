#pragma once
#include "stdafx.h"

class DLLEXPORT IL_Timer
{
public:
	IL_Timer(int seconds);
	~IL_Timer();

	//starts the timer count down
	void Start();
	//resets the timer with the same timer as previously
	void Reset();
	//resets the timer with new time
	void Reset(int newSeconds);
	//pauses the timer
	void Pause();
	//resumes the timer if it was stopped
	void Resume();
	//stops the timer
	void Stop();

	//returns true if timer has started
	bool isStarted();
	//returns true if timer is finished
	bool isFinished();
	//returns true if timer is paused
	bool isPaused();

	//return the original time lenght at start of timer
	int GetStartTime();

	//return time remaining in milliseconds
	int GetTimeRemaining();
private:
	//resets the pause variables
	void ResetPauseTimers();

	//how much time has passed while its been paused
	int _timeElapsedWhilePaused;
	//the time at current pause
	int _timeAtStartPause; 
	//how much time has elapsed since start of timer
	int _timeToElapse;
	//time when timer has started
	Uint32 _startTicks;
	
	bool _started;
	bool _paused;
	bool _finished;
};
