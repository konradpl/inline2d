#pragma once
#include "MultiPlatformDefines.h"

class DLLEXPORT Vector3i
{
public:
	Vector3i();
	Vector3i(int x, int y, int z);
	~Vector3i();
	int x;
	int y;
	int z;
};
