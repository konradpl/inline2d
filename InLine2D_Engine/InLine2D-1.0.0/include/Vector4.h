#pragma once
#include <SDL_rect.h>

class __declspec(dllexport) Vector4
{
public:
	Vector4() { x = 0; y = 0; w = 0; h = 0; }
	Vector4(int x, int y, int z) { this->x = x; this->y = y; this->w = w; this->h = h; }
	~Vector4() {}
	SDL_Rect ToSDLRect() { return SDL_Rect{ x, y, w, h }; }
	int x;
	int y;
	int w;
	int h;
};
