#pragma once
#include "stdafx.h"
#include "Button.h"
#include <vector>

class DLLEXPORT GUIManager
{
public:
	GUIManager();
	~GUIManager();

	void AddObjectToListen(IL_Button* objToListen);
	//void RemoveObjectFromListening(IL_Object* objToRemove);
	void Update(SDL_Event* events);
private:
	Vector2i _mousePosition;
	std::vector<IL_Button*>* _objectListened;
};

