#pragma once
#include "MultiPlatformDefines.h"
#include <iostream>
#ifdef __WINDOWS__
#include <Windows.h>
#endif

class DLLEXPORT Console
{
public:

#define CONSOLE_ERROR 12
#define CONSOLE_WARNING 14

	//get console instance
	static Console& instance()
	{
		static Console INSTANCE;
		return INSTANCE;
	}

	//print text to console, with 1 arguments
	template <class T>
	void Print(T arg1, int logType = 7)
	{
#ifdef __WINDOWS__
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), logType);
#endif
		std::cout << arg1 << std::endl;
	}

	//print text to console, with 2 arguments
	template <class T, class V>
	void Print(T arg1, V arg2, int logType = 7)
	{
#ifdef __WINDOWS__
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), logType);
#endif
		std::cout << arg1 << " " << arg2 << std::endl;
	}

private:
	Console() {}
	~Console() {}
};

