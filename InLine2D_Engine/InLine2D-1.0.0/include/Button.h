#pragma once
#include "stdafx.h"
#include "Text.h"
#include "Texture.h"

class DLLEXPORT IL_Button : public IL_Object
{
public:
	IL_Button(char* defaultButtonFilePath, char* pressedButtonFilePath, char* disabledButtonFilePath, TTF_Font* font, Color defaultColor, Color pressedColor, Color disabledColor, Vector2f position, Vector2f size, SDL_Renderer* renderer, float* deltaTime, char* contentText = "");
	~IL_Button();
	//draws the button to the screen
	void Draw();
	bool isPressed();
	void SetPressed(bool toggle);

	void SetPosition(Vector2f position);
	void Rotate(double angle);
	void SetDisable(bool toggle);

	void SetTextureUp();
	void SetTextureDown();
	void SetTextureDisable();

	IL_Text* GetTextData();

private:
	float *_deltaTime;
	bool _disabled;
	bool _pressed;
	SDL_Renderer* _Renderer;
	IL_Text* buttonText;

	struct buttonTextColors {
		Color default;
		Color pressed;
		Color disabled;
	};

	buttonTextColors _buttonTextColors;
	IL_Texture* _defaultButtonTexture;
	IL_Texture* _pressedButtonTexture;
	IL_Texture* _disabledButtonTexture;

	SDL_Texture* _currentTexture;
};

