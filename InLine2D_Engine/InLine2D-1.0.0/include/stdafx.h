#pragma once
//graphics libs
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h> 
#include <SDL_ttf.h>

//Inline General libs
#include "MultiPlatformDefines.h"
#include "Utility.h"
#include "Color.h"
#include "Vector2i.h"
#include "Vector2f.h" 
#include "Vector3i.h"
#include "Vector3f.h"
#include "Vector4i.h"
#include "Vector4f.h"
#include "Object.h"

