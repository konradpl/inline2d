#pragma once
#include "stdafx.h"
#include "Console.h"
#include <map>


class DLLEXPORT Audio
{
public:
	Audio();
	~Audio();

	//add music track to music list
	void AddMusicTrack(char* filePath, char* oundName);
	//add sound effect to sound list
	void AddSoundEffect(char*  filePath, char*  soundName);
	//play specific sound effect, and set if it loops
	void PlaySoundEffect(char*  soundName, int loops = 0);
	//play specific music track, and set if it loops (automatically it will loop forever, set to 0 if want to play once)
	void PlayMusic(char*  musicName, int loops = -1);
	//stop playing music track
	void StopMusic();
	//pause music track
	void PauseMusic();
	//resume from where music track was paused
	void ResumeMusic();
	//set volume of music
	void SetVolumeMusic(float volume);
	//set volume of specific sound
	void SetVolumeSoundEffect(char*  soundName, float volume);
private:
	std::map<char*, Mix_Music*> *_MusicTrack; 
	std::map<char*, Mix_Chunk*> *_SoundsEffects; 
};

