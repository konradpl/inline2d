#pragma once
#include <stdlib.h>
#include <time.h>
#include <sstream>

class DLLEXPORT Utility
{
private:
	Utility() { srand((unsigned int)time(NULL)); }
	~Utility() {}
public:
	//get console instance
	static Utility& instance()
	{
		static Utility INSTANCE;
		return INSTANCE;
	}

	template <class T>
	T RandomRange(T min, T max)
	{
		return ((max - min)*((float)rand() / RAND_MAX)) + min;
	}

	template <class T>
	std::string ToString(T toConvert)
	{
		std::ostringstream ss;
		ss << toConvert;
		return ss.str();
	}



};