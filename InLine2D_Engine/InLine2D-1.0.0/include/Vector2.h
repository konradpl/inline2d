class __declspec(dllexport) Vector2
{
public:
	Vector2() { x = 0; y = 0; }
	Vector2(int x, int y){ this->x = x; this->y = y; }
	~Vector2() {}
	SDL_Point ToSDLPoint() { return SDL_Point{ x, y}; }
	int x;
	int y;
};
