#pragma once
#include "stdafx.h"
#include "Console.h"
#include <string.h>
class DLLEXPORT IL_Text : public IL_Object
{
public:
	IL_Text();
	IL_Text(TTF_Font* font, Color color, Vector2f position, SDL_Renderer* renderer, float* deltaTime, char* contentText = "", unsigned short wrapWidth = 0);
	~IL_Text();
	void SetText(const char* text, unsigned short wrapWidth = 0);
	//Set Color
	void SetColor(Color color);
	//Set Font
	void SetFont(TTF_Font* font);
	//drawing text to the screen
	void Draw();
	//resetting up information about text, if you want to change font or color
	void SetupText(TTF_Font* font, Color color, Vector2f position, char* contentText = "", unsigned short wrapWidth = 0);

protected:
	std::string *_content;
	TTF_Font* _font;
	SDL_Texture* _text_texture;
	Color _color;
	SDL_Renderer* _Renderer;
};

