#pragma once

#include "stdafx.h"

class DLLEXPORT IL_Rectangle
{
public:
	IL_Rectangle(Vector2i position, Vector2i size, Color color, Color color_border, SDL_Renderer* renderer);
	~IL_Rectangle();

	void Draw();
	void SetPosition(Vector2i position);
	void SetSize(Vector2i size);
	void SetColor(Color color);
	void SetColorBorder(Color color_border);

private:
	SDL_Rect _rect;
	SDL_Renderer *_Renderer;
	Color _color;
	Color _color_border;
};

