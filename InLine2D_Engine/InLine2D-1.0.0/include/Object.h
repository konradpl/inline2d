#pragma once
#include "stdafx.h"

class DLLEXPORT IL_Object
{
public:
	IL_Object();
	~IL_Object();

	//move object by values (use in OnUpdate())
	void Move(float x, float y);
	//sets position of object
	virtual void SetPosition(Vector2f position);
	//set X Position
	void SetX(float x);
	//set Y Position
	void SetY(float y);
	//get position Vector
	Vector2f GetPosition();
	//gets X position of object
	float GetX();
	//gets Y position of object
	float GetY();
	//sets size of the object, centerPivot automatically adjusts pivot based on size
	void SetSize(Vector2f size, bool centerPivot = true);
	//set Width of object
	void SetW(float width, bool centerPivot = true);
	//set Height of object
	void SetH(float height, bool centerPivot = true);
	//gets size of the object
	Vector2f GetSize();
	//gets width of object
	float GetW();
	//gets height of object
	float GetH();
	//rotate of the object
	virtual void Rotate(double angle);
	//sets angle of the object
	void SetAngle(double angle);
	//gets angle of the object
	double GetAngle();
	//sets the pivot point of the object
	void SetPivot(Vector2f pivot);
	//sets the pivot x position of the object
	void SetPivotX(float x);
	//sets the pivot x position of the object
	void SetPivotY(float y);
	//gets the x position of the object's pivot
	float GetPivotX();
	//gets the y position of the object's pivot
	float GetPivotY();
	//Flipping image: SDL_FLIP_HORIZONTAL, SDL_FLIP_VERTICAL, SDL_FLIP_NONE
	void SetFlipImage(int SDL_FLIP_ID);
	//get if image is flipped in any way
	SDL_RendererFlip GetFlipImage();
protected:
	void SetDeltaTime(float *deltaTime);
	float *_deltaTime;

	Vector4f _imageRect;
	Vector2i _imageSize;

	double _angle;
	Vector2f _imagePivot;
	SDL_RendererFlip _imageFlip;
};

