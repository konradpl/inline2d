#pragma once
#include "MultiPlatformDefines.h"
#include <SDL_rect.h>
#include "Vector2f.h"

class DLLEXPORT Vector2i
{
public:
	Vector2i();
	Vector2i(int x, int y);
	~Vector2i();
	Vector2f ToFloat();
	SDL_Point ToSDLPoint();
	int x;
	int y;
};
