#include "Vector4i.h"

Vector4i::Vector4i()
{
	x = 0;
	y = 0;
	w = 0;
	h = 0;
}

Vector4i::Vector4i(int x, int y, int w, int h)
{
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
}

Vector4i::~Vector4i()
{

}

SDL_Rect Vector4i::ToSDLRect()
{
	SDL_Rect rect = { x, y, w, h };
	return rect;
}