#include "Text.h"

IL_Text::IL_Text()
{

}

IL_Text::IL_Text(TTF_Font* font, Color color, Vector2f position, SDL_Renderer* renderer, float* deltaTime, char* contentText, unsigned short wrapWidth)
{
	SetDeltaTime(deltaTime);
	_content = new std::string;
	_Renderer = renderer;
	SetupText(font, color, position, contentText, wrapWidth);
}

IL_Text::~IL_Text()
{
	delete _content;
	SDL_DestroyTexture(_text_texture);
	_text_texture = NULL;
}

void IL_Text::SetupText(TTF_Font* font, Color color, Vector2f position, char* contentText, unsigned short wrapWidth)
{
	SDL_DestroyTexture(_text_texture);
	_text_texture = NULL;

	_font = font;
	*_content = contentText;
	_color = color;
	SDL_Color _SDLcolor;
	_SDLcolor.a = (int)(color.alpha * 254); _SDLcolor.r = (int)(color.red * 254);	_SDLcolor.b = (int)(color.blue * 254);	_SDLcolor.g = (int)(color.green * 254);

	SDL_Surface *surf;
	wrapWidth ? surf = TTF_RenderText_Blended_Wrapped(_font, contentText, _SDLcolor, wrapWidth) : 
		surf = TTF_RenderText_Blended(_font, contentText, _SDLcolor);
	
	_text_texture = SDL_CreateTextureFromSurface(_Renderer, surf);
	SDL_FreeSurface(surf);


	int w, h;
	SDL_QueryTexture(_text_texture, NULL, NULL, &w, &h);
	SetSize(Vector2f((float)w,(float)h));
	SetPosition(position);
}

void IL_Text::Draw()
{
	SDL_RenderCopyEx(_Renderer, _text_texture, NULL, &_imageRect.ToSDLRect(), _angle, &_imagePivot.ToSDLPoint(), _imageFlip);
}

void IL_Text::SetText(const char* text, unsigned short wrapWidth)
{
	if (*_content != text)
	{
		SetupText(_font, _color, GetPosition(), (char*)text, wrapWidth);
	}
}

void IL_Text::SetColor(Color color)
{
	if (_color.red != color.red || _color.green != color.green || _color.blue != color.blue || _color.alpha != color.alpha)
	{
		SetupText(_font, color, GetPosition(), (char*)(*_content).c_str());
	}
}

void IL_Text::SetFont(TTF_Font* font)
{
	if (_font != font)
	{
		SetupText(font, _color, GetPosition(), (char*)(*_content).c_str());
	}
}