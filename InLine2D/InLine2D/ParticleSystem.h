//#pragma once
//#include "stdafx.h"
//#include "Texture.h"
//#include "Particle.h" 
//#include <vector>
//class __declspec(dllexport) IL_ParticleSystem
//{
//public:
//	IL_ParticleSystem(Vector2i particlePosition, float particleSize, float particleLifeTime, float particleSpawnRate, int particleAmount, IL_Texture* texture, SDL_Renderer *renderer, float* deltaTime);
//	~IL_ParticleSystem();
//
//	void Draw();
//	void SetVelocity(Vector2i velocity);
//	void SetPosition(Vector2i position);
//	void SetAlpha(float alpha);
//	void SetAngle(int angle);
//	void SetAreaOffset(Vector2i offset, Vector2i maxOffset);
//
//private:
//	float* _deltaTime;
//	float _timeCounter;
//	int _particleAmount;
//	int _alpha;
//	int _angle;
//	Vector2i _velocity;
//	std::vector<IL_Particle *>* _particleList;
//	SDL_Renderer* _renderer;
//	
//	Vector2i _particlePosition;
//	float _particleSize;
//	IL_Texture* _texture;
//	float _particleLifeTime;
//	float _particleSpawnRate;
//	Vector2i _minOffset;
//	Vector2i _maxOffset;
//};
//
