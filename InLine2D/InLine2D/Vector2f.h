#pragma once
#include "MultiPlatformDefines.h"
#include <SDL_rect.h>

class DLLEXPORT Vector2f
{
public:
	Vector2f();
	Vector2f(float x, float y);
	~Vector2f();
	SDL_Point ToSDLPoint();
	float x;
	float y;
};
