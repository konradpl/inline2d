#include "Object.h"



IL_Object::IL_Object()
{
}


IL_Object::~IL_Object()
{
}

void IL_Object::SetDeltaTime(float *deltaTime)
{
	_deltaTime = deltaTime;
}

void IL_Object::Move(float x, float y)
{
	_imageRect.x += x * *_deltaTime;
	_imageRect.y += y * *_deltaTime;
}

void IL_Object::SetPosition(Vector2f position)
{
	_imageRect.x = position.x - _imagePivot.x;
	_imageRect.y = position.y - _imagePivot.y;
}

void IL_Object::SetX(float x)
{
	_imageRect.x = x - _imagePivot.x;
}

void IL_Object::SetY(float y)
{
	_imageRect.y = y - _imagePivot.y;
}

Vector2f IL_Object::GetPosition()
{
	return Vector2f(_imageRect.x + _imagePivot.x, _imageRect.y + _imagePivot.y);
}

float IL_Object::GetX()
{
	return _imageRect.x + _imagePivot.x;
}
float IL_Object::GetY()
{
	return _imageRect.y + _imagePivot.y;
}

void IL_Object::SetSize(Vector2f size, bool centerPivot)
{
	_imageRect.w = size.x;
	_imageRect.h = size.y;
	if (centerPivot) {
		_imagePivot.x = _imageRect.w / 2;
		_imagePivot.y = _imageRect.h / 2;
	}
}

void IL_Object::SetW(float width, bool centerPivot)
{
	_imageRect.w = width;
	if (centerPivot)
		_imagePivot.x = _imageRect.w / 2;
}
void IL_Object::SetH(float height, bool centerPivot)
{
	_imageRect.h = height;
	if (centerPivot)
		_imagePivot.y = _imageRect.h / 2;
}

Vector2f IL_Object::GetSize()
{
	return  Vector2f(_imageRect.w, _imageRect.h);
}

float IL_Object::GetH()
{
	return _imageRect.h;
}
float IL_Object::GetW()
{
	return _imageRect.w;
}

void IL_Object::Rotate(double angle)
{
	_angle += angle * *_deltaTime;
}

void IL_Object::SetAngle(double angle)
{
	_angle = angle;
}

double IL_Object::GetAngle()
{
	return _angle;
}

void IL_Object::SetPivot(Vector2f pivot)
{
	_imagePivot.x = pivot.x;
	_imagePivot.y = pivot.y;
}

void IL_Object::SetPivotX(float x)
{
	_imagePivot.x = x;
}

void IL_Object::SetPivotY(float y)
{
	_imagePivot.y = y;
}

float IL_Object::GetPivotX()
{
	return _imagePivot.x;
}

float IL_Object::GetPivotY()
{
	return _imagePivot.y;
}

void IL_Object::SetFlipImage(int SDL_FLIP_ID)
{
	_imageFlip = (SDL_RendererFlip)SDL_FLIP_ID;
}

SDL_RendererFlip IL_Object::GetFlipImage()
{
	return _imageFlip;
}