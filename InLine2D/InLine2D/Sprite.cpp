#include "Sprite.h"

IL_Sprite::IL_Sprite(Vector2f position, Vector2f scale, char* fileName, SDL_Renderer* renderer, float* deltaTime)
{
	_deleteTextureLoader = true;
	_textureLoader = new IL_Texture(renderer);
	_textureLoader->LoadImage(fileName);

	Initialize(renderer, deltaTime);
	SetSize(Vector2f(scale.x, scale.y));
	SetPosition(Vector2f(position.x, position.y));
}									    

IL_Sprite::IL_Sprite(Vector2f position, char* fileName, SDL_Renderer* renderer, float* deltaTime)
{
	_deleteTextureLoader = true;
	_textureLoader = new IL_Texture(renderer);
	_textureLoader->LoadImage(fileName);

	Initialize(renderer, deltaTime);
	int w, h;
	//getting the size of loaded image and 
	SDL_QueryTexture(_textureLoader->GetTexture(), NULL, NULL, &w, &h);
	SetSize(Vector2f((float)w, (float)h));

	SetPosition(Vector2f(position.x, position.y));
}

IL_Sprite::IL_Sprite(Vector2f position, Vector2f scale, IL_Texture* texture, SDL_Renderer* renderer, float* deltaTime)
{
	_deleteTextureLoader = false;
	_textureLoader = texture;
	Initialize(renderer, deltaTime);
	SetSize(Vector2f(scale.x, scale.y));
	SetPosition(Vector2f(position.x, position.y));
}

IL_Sprite::IL_Sprite(Vector2f position, IL_Texture* texture, SDL_Renderer* renderer, float* deltaTime)
{
	_deleteTextureLoader = false;
	_textureLoader = texture;
	Initialize(renderer, deltaTime);
	int w, h;
	//getting the size of loaded image and 
	SDL_QueryTexture(_textureLoader->GetTexture(), NULL, NULL, &w, &h);
	SetSize(Vector2f((float)w, (float)h));
	SetPosition(Vector2f(position.x, position.y));
}
IL_Sprite::~IL_Sprite()
{
	if (_deleteTextureLoader)
	{
		delete _textureLoader;
	}
	delete _animationlist;
}

void IL_Sprite::Initialize(SDL_Renderer* renderer, float *deltaTime)
{
	_deltaTime = deltaTime;
	_Renderer = renderer;
	_angle = 0;
	_animationlist = new std::map<char*, Animation>;
	
	_Frames.x = 1;
	_Frames.y = 1;

	SDL_QueryTexture(_textureLoader->GetTexture(), NULL, NULL, &_imageSize.x, &_imageSize.y);
	_sourceRect.x = 0;	_sourceRect.y = 0;	_sourceRect.w = _imageSize.x;	_sourceRect.h = _imageSize.y;
	_animTimeTracking = SDL_GetTicks();

	_colliderList = new IL_ColliderList(&_imageRect.x, &_imageRect.y);
	SetDrawColliders(false);
}

void IL_Sprite::Draw()
{
	SDL_RenderCopyEx(_Renderer, _textureLoader->GetTexture(), &_sourceRect, &_imageRect.ToSDLRect(), _angle, &_imagePivot.ToSDLPoint(), _imageFlip);
	if (_drawColliders)
	{
		SDL_SetRenderDrawColor(_Renderer, 0, 255, 0, 255);
		for (auto const &_colliderRect : _colliderList->GetColliderList())
		{
			Vector4f tempRect = _colliderRect.second;
			tempRect.x += _imageRect.x;
			tempRect.y += _imageRect.y;
			SDL_RenderDrawRect(_Renderer, &tempRect.ToSDLRect());
		}
		SDL_SetRenderDrawColor(_Renderer, 0, 0, 0, 255);
	}
}


void IL_Sprite::SetupSpriteSheet(int tatalFramesX, int totalFramesY)
{
	_Frames.x = tatalFramesX;
	_Frames.y = totalFramesY;

	SetAnimationFrame(0, 0);
}

void IL_Sprite::SetAnimationFrame(int xFrame, int yFrame)
{
	_sourceRect.x = (_imageSize.x / _Frames.x) * xFrame;
	_sourceRect.y = (_imageSize.y / _Frames.y) * yFrame;
	_sourceRect.w = _imageSize.x / _Frames.x;
	_sourceRect.h = _imageSize.y / _Frames.y;
}

void IL_Sprite::AddAnimation(char* animName, int startFrameX, int endFrameX, int yFrame)
{
	if ((*_animationlist).find(animName) == (*_animationlist).end())
	{
		Animation newAnimation;
		_currentAnimFrame = startFrameX;
		newAnimation.startFrameX = startFrameX;
		newAnimation.endFrameX = endFrameX;
		newAnimation.yFrame = yFrame;
		(*_animationlist)[animName] = newAnimation;
	}
	else
		Console::instance().Print("(InLine2D) - Failed at AddAnimation, animation already exists: ", animName, CONSOLE_WARNING);
}

void IL_Sprite::RemoveAnimation(char* animName)
{
	if ((*_animationlist).find(animName) != (*_animationlist).end())
		(*_animationlist).erase(animName);
	else
		Console::instance().Print("(InLine2D) - Failed at RemoveAnimation, animation doesn't exist: ", animName, CONSOLE_WARNING);
}

int IL_Sprite::GetCurrentAnimationFrame()
{
	return _currentAnimFrame;
}

void IL_Sprite::SetCurrentAnimationFrame(int frame)
{
	_currentAnimFrame = frame;
}

void IL_Sprite::PlayAnimation(char* animName, float delay)
{
	if ((*_animationlist).find(animName) != (*_animationlist).end())
	{
		if (_animTimeTracking + delay < (SDL_GetTicks()))
		{
			_animTimeTracking = SDL_GetTicks();
			_currentAnimFrame++;

			if ((*_animationlist)[animName].endFrameX < _currentAnimFrame)
			{
				_currentAnimFrame = (*_animationlist)[animName].startFrameX;
			}

			SetAnimationFrame(_currentAnimFrame, (*_animationlist)[animName].yFrame);
		}
	}
	else
		Console::instance().Print("(InLine2D) - Failed at PlayAnimation, animation doesn't exist: ", animName, CONSOLE_WARNING);
}

double IL_Sprite::GetDistance(Vector2i positionB)
{
	return sqrt((positionB.x - GetPosition().x)*(positionB.x - GetPosition().x) + (positionB.y - GetPosition().y)*(positionB.y - GetPosition().y));
}

void IL_Sprite::AddCollider(char* colliderName)
{
	_colliderList->AddCollider(colliderName, Vector4f(0, 0, _imageRect.w, _imageRect.h));
}

void IL_Sprite::AddCollider(char* colliderName, float w, float h)
{
	_colliderList->AddCollider(colliderName, Vector4f((float)(_imagePivot.x - w/2), (float)(_imagePivot.y - h/2), w, h));
}

void IL_Sprite::AddCollider(char* colliderName, float x, float y, float w, float h)
{
	_colliderList->AddCollider(colliderName, Vector4f((float)((_imagePivot.x - w / 2) + x), (float)((_imagePivot.y - h/2) + y), w, h));
}

void IL_Sprite::RemoveCollider(char* colliderName)
{
	_colliderList->RemoveCollider(colliderName);
}

Vector4f IL_Sprite::GetCollider(char* colliderName)
{
	return _colliderList->GetCollider(colliderName);
}

bool IL_Sprite::DetectCollision(char* colliderName, Vector4f collider)
{
	return _colliderList->DetectCollision(colliderName, collider);
}

bool IL_Sprite::DetectCollision(Vector4f collider)
{
	return _colliderList->DetectCollision(collider);
}

void IL_Sprite::SetDrawColliders(bool toggle)
{
	_drawColliders = toggle;
}

bool IL_Sprite::GetDrawColliders()
{
	return _drawColliders;
}