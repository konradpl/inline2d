#pragma once
#include "MultiPlatformDefines.h"
#include <SDL_rect.h>

class DLLEXPORT Vector4i
{
public:
	Vector4i();
	Vector4i(int x, int y, int w, int h);
	~Vector4i();
	SDL_Rect ToSDLRect();
	int x;
	int y;
	int w;
	int h;
};
