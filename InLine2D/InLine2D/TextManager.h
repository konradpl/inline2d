#pragma once
#include "stdafx.h"
#include "Console.h"
#include <map>
class DLLEXPORT TextManager
{ 
public:  
	TextManager();
	~TextManager();
	 //add font to font manager so can be used inside IL_Text for future references
	void AddFont(char*  filePath, char*  fontIDname, int fontSize);
	//removes font from font manager, you do not have to use it in OnClose as engine will remove all fonts automatically once game closes
	void RemoveFont(char* fontIDname);
	//gets the font (use it in IL_Text)
	TTF_Font* GetFont(char* fontIDname);
private:
	std::map <char*, TTF_Font*> *_fontList; 
};

