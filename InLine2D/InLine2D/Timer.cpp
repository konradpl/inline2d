#include "Timer.h"

IL_Timer::IL_Timer(int secondsToElapse)
{
	_paused = false;
	_started = false;
	_finished = false;
	ResetPauseTimers();
	_timeToElapse = secondsToElapse;
}

IL_Timer::~IL_Timer()
{

}

void IL_Timer::Start()
{
	_paused = false;
	_started = true;
	_finished = false;
	ResetPauseTimers();
	_startTicks = SDL_GetTicks();
}

void IL_Timer::Reset()
{
	if (!_started) return;
	_finished = false;
	_startTicks = SDL_GetTicks();
}

void IL_Timer::Reset(int newSeconds)
{
	if (!_started) return; 
	_finished = false;
	_startTicks = SDL_GetTicks();
	_timeToElapse = newSeconds;
}

void IL_Timer::Pause()
{
	if (isPaused() || !_started || _finished) return;

	_timeAtStartPause = (int)(SDL_GetTicks() - _startTicks);
	_paused = true;
}

void IL_Timer::Resume()
{
	if (!_started || isFinished()) return;

	if (_paused)
	{
		_timeElapsedWhilePaused = (SDL_GetTicks() - _startTicks) - _timeAtStartPause;
		_paused = false;
	}
}

void IL_Timer::Stop()
{
	if (!_started || isFinished()) return;

	_started = false;
	_paused = false;
	_startTicks = 0;
}

int IL_Timer::GetStartTime()
{
	return _timeToElapse;
}

int IL_Timer::GetTimeRemaining()
{
	if (!_started) return _timeToElapse;
	if (isFinished()) return 0;

	if (isPaused())
	{
		int timeRemaining = (_timeToElapse * 1000) - _timeAtStartPause;
		return timeRemaining > 0 ? timeRemaining : 0;
	}

	int timeElapsed = (SDL_GetTicks() - _timeElapsedWhilePaused) - _startTicks;
	return (_timeToElapse * 1000) - timeElapsed;	
}

bool IL_Timer::isFinished()
{
	if (!_started) return false;
	if (_finished) return true;
	if (_paused) return false;

	int _timeElapsed = (SDL_GetTicks() - _timeElapsedWhilePaused) - _startTicks;
	if (_timeElapsed > (_timeToElapse * 1000))
	{
		ResetPauseTimers();
		_finished = true;
		return true;
	}
	
	return false;
}

bool IL_Timer::isStarted()
{
	return _started;
}

bool IL_Timer::isPaused()
{
	return _paused;
}

void IL_Timer::ResetPauseTimers()
{
	_timeElapsedWhilePaused = 0;
	_timeAtStartPause = 0;
}