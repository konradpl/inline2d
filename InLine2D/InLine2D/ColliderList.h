#pragma once
#include "stdafx.h"
#include "Console.h"
#include <map>

class IL_ColliderList
{
public:
	//Created Colliders will be at static specified position
	IL_ColliderList();
	//Created Colliders will follow the object based n the passed in position values
	IL_ColliderList(float *followX, float *followY);
	~IL_ColliderList();
	//add a collider to the list and give it a name
	void AddCollider(char* colliderName, Vector4f collider);
	//get collider with the specified name
	Vector4f GetCollider(char* colliderName);
	//get whole collider list
	std::map<char*, Vector4f> GetColliderList();
	//remove collider collider from the list with specific name
	void RemoveCollider(char* colliderName);
	//remove all colliders from the list
	void ClearColliderList();
	//detect collision between with the specific named collider
	bool DetectCollision(char* colliderName, Vector4f collider);
	//detect collision with all colliders in the list
	bool DetectCollision(Vector4f collider);
private:
	std::map<char*, Vector4f> _colliderList;
	struct FollowPosition { 
		float *x; 
		float *y; 
	};
	FollowPosition _follow;
};

