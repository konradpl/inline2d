#include "Color.h"

Color::Color()
{
	red = 0;
	green = 0;
	blue = 0;
	alpha = 1;
}

Color::Color(float r, float g, float b, float a)
{
	red = r;
	green = g;
	blue = b;
	alpha = a;
}

Color::Color(float r, float g, float b) 
{
	red = r;
	green = g;
	blue = b;
	alpha = 1.0f;
}

Color::~Color()
{

}

