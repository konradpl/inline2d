#include "Vector2i.h"

Vector2i::Vector2i()
{
	 x = 0; 
	 y = 0; 
}

Vector2i::Vector2i(int x, int y)
{
	this->x = x; 
	this->y = y;
}

Vector2i::~Vector2i()
{
	   
}

Vector2f Vector2i::ToFloat()
{
	return Vector2f((float)x, (float)y);
}
 
SDL_Point Vector2i::ToSDLPoint()
{
	SDL_Point point = { x, y };
	return point;
}