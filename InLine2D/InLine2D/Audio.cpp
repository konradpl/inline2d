#include "Audio.h"

Audio::Audio()
{
	_SoundsEffects = new std::map<char*, Mix_Chunk*>;
	_MusicTrack = new std::map<char*, Mix_Music*>;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		Console::instance().Print("(InLine2D) - SDL_mixer could not initialize!\n  *Error: ", Mix_GetError(), CONSOLE_ERROR);
	}
}

Audio::~Audio()
{
	//clean all the Sound Effects from memory
	for (auto const &ent1 : *_SoundsEffects) {
		Mix_FreeChunk(ent1.second); 
	}
	_SoundsEffects->clear();

	delete _SoundsEffects;
	//clean all the Music Tracksfrom memory
	for (auto const &ent1 : *_MusicTrack) 
	{
		Mix_FreeMusic(ent1.second);
	}

	_MusicTrack->clear();

	delete _MusicTrack;

	Mix_Quit();
}

void Audio::PlaySoundEffect(char* soundName, int loops)
{
	if ((*_SoundsEffects)[soundName] != NULL)
		Mix_PlayChannel(-1, (*_SoundsEffects)[soundName], loops);
	else
		Console::instance().Print("(InLine2D) - Failed to play sound effect. SoundName does not exist: ", soundName, CONSOLE_WARNING);
}

void Audio::PlayMusic(char*  musicName, int loops)
{
		if ((*_MusicTrack)[musicName] != NULL)
			Mix_PlayMusic((*_MusicTrack)[musicName], loops);
		else
			Console::instance().Print("(InLine2D) - Failed to play music. MusicName does not exist: ", musicName , CONSOLE_WARNING);
}

void Audio::AddMusicTrack(char* filePath, char* musicName)
{
	(*_MusicTrack)[musicName] = Mix_LoadMUS(filePath);

	if ((*_MusicTrack)[musicName] == NULL)
		Console::instance().Print("(InLine2D) - Failed to load Music Track!\n  *Error: ", Mix_GetError(), CONSOLE_ERROR);
	else
		Console::instance().Print("(InLine2D) - Successfully loaded Music: ", filePath);
}

void Audio::AddSoundEffect(char* filePath, char* soundName)
{
	(*_SoundsEffects)[soundName] = Mix_LoadWAV(filePath);

	if ((*_SoundsEffects)[soundName] == NULL)
		Console::instance().Print("(InLine2D) - Failed to load Sound Effect!\n  *Error: ", Mix_GetError(), CONSOLE_ERROR);
	else
		Console::instance().Print("(InLine2D) - Successfully loaded sound effect: ", filePath);
}


void Audio::StopMusic()
{
	Mix_HaltMusic();
}

void Audio::PauseMusic()
{
	Mix_PauseMusic();
}

void Audio::ResumeMusic()
{
	Mix_ResumeMusic();
}

void Audio::SetVolumeMusic(float volume)
{
	int volumeSDL = (int)(volume * 128);
	Mix_VolumeMusic(volumeSDL);
}

void Audio::SetVolumeSoundEffect(char* soundName, float volume)
{
	int volumeSDL = (int)(volume * 128);
	Mix_VolumeChunk((*_SoundsEffects)[soundName], volumeSDL);
}