#include "inLine2D.h"
using namespace iL2D;

inLine2D::inLine2D()
{
	_deltaTime = new float;
	_endApplication = false;
	_frameRateCapEnabled = false;
	MAX_FPS = 30; // 30 fps default
}

inLine2D::~inLine2D()
{

}

void inLine2D::IL_StartApplication(int width, int height, bool fullScreen, char* windowName)
{
	_screenInformation.width = width;
	_screenInformation.height = height;
	_screenInformation.fullScreen = fullScreen;
	_screenInformation.windowName = windowName;
	_screenInformation.windowType = fullScreen ? SDL_WINDOW_FULLSCREEN : SDL_WINDOW_SHOWN;

	inLine2D_Initialize();
	inLine2D_Update();
	inLine2D_Close();
}

bool inLine2D::inLine2D_Initialize()
{
	//Initialization flag
	bool success = true;
	
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER) < 0)
	{
		Console::instance().Print("(inLine2D) - Could not initialize SDL. \n *Error: ", SDL_GetError(), CONSOLE_ERROR);
		success = false;
	}
	else
	{
		_Window = SDL_CreateWindow(_screenInformation.windowName, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			_screenInformation.width, _screenInformation.height, _screenInformation.windowType);
		UpdateScreenResolution();//correct window resolution if it was not changed

		if (_Window == NULL)
		{
			Console::instance().Print("(inLine2D) - Window could not be created. \n *Error: ", SDL_GetError(), CONSOLE_ERROR);
			success = false;
		}
		else
		{
			_Renderer = SDL_CreateRenderer(_Window, -1, SDL_RENDERER_ACCELERATED); //SDL_RENDERER_PRESENTVSYNC add?
		}
	}

	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
	{
		Console::instance().Print("(inLine2D) - IMG failed to initialize. \n *Error: ", SDL_GetError(), CONSOLE_ERROR);
		success = false;
	}
	
	_AudioManager = new Audio();
	_InputManager = new Input(&_screenInformation.width, &_screenInformation.height);
	_TextManager = new TextManager();
	_GUIManager = new GUIManager();

	if (success)
		Console::instance().Print("(inLine2D) - Initalized Successfully...");
	else
		Console::instance().Print("(inLine2D) - Initalized Failed...", CONSOLE_ERROR);
		
	OnInitialize();

	return success;
}

void inLine2D::inLine2D_Close()
{	
	OnClose();
	Console::instance().Print("(inLine2D) - Closing...");
	delete _AudioManager;
	delete _InputManager;
	delete _TextManager;
	delete _GUIManager;
	delete _deltaTime;
	SDL_DestroyWindow(_Window);
	_Window = NULL;
	SDL_DestroyRenderer(_Renderer);
	_Renderer = NULL;
	IMG_Quit();
	SDL_Quit();
}

void inLine2D::IL_EndApplication()
{
	_endApplication = true;
}

void inLine2D::IL_SetWindowResolution(int width, int height)
{
	SDL_SetWindowSize(_Window, width, height);
	UpdateScreenResolution();//update windows resolution based on new resolution if it changed successfully 
}

void inLine2D::IL_SetFullScreen(bool toggle)
{
	_screenInformation.windowType = toggle ? SDL_WINDOW_FULLSCREEN : SDL_WINDOW_SHOWN;
	_screenInformation.fullScreen = toggle;

	SDL_SetWindowFullscreen(_Window, _screenInformation.windowType);
}

void inLine2D::IL_SetFrameRateCap(int FPS)
{
	_frameRateCapEnabled = true;
	MAX_FPS = FPS;
}

void inLine2D::IL_ToggleFrameRateCap(bool toggle)
{
	_frameRateCapEnabled = toggle;
}

void inLine2D::UpdateTime()
{
	//update time information
	_oldTime = _currentTime;
	_currentTime = SDL_GetTicks();
	*_deltaTime = (_currentTime - _oldTime) / 1000.0f;

	//cap framerate
	if (_frameRateCapEnabled)
	if ((SDL_GetTicks() - _currentTime) < (1000 / (MAX_FPS)))
	{
		SDL_Delay(((1000 / MAX_FPS) - (SDL_GetTicks() - _currentTime)));
	}
}

void inLine2D::inLine2D_Update()
{
	//storing SDL event
	SDL_Event e;

	//main game loop
	while (!_endApplication)
	{
		//updating the time, and setting framerate cap
		UpdateTime();

		//looping throught SDL events
		while (SDL_PollEvent(&e) != 0) 	
		{
			//checking if user has tried to quit application
			if (e.type == SDL_QUIT) _endApplication = true;	

			//updating the keyboard and mouse input
			_InputManager->Update(&e);
			_GUIManager->Update(&e);
		}
		SDL_RenderClear(_Renderer);

		//Drawing all the textures/sprites to screen
		OnDraw();
		//Updating the game logic
		OnUpdate();

		SDL_RenderPresent(_Renderer);
	}
}

void inLine2D::UpdateScreenResolution()
{
	SDL_GetWindowSize(_Window, &_screenInformation.width, &_screenInformation.height);
}

IL_Sprite* inLine2D::IL_CreateSprite(Vector2f position, Vector2f scale, char* fileName)
{
	return new IL_Sprite(position, scale, fileName, IL_GetRenderer(), IL_GetDeltaTime());
}
IL_Sprite* inLine2D::IL_CreateSprite(Vector2f position, char* fileName)
{
	return new IL_Sprite(position, fileName, IL_GetRenderer(), IL_GetDeltaTime());
}

IL_Sprite* inLine2D::IL_CreateSprite(Vector2f position, Vector2f scale, IL_Texture* texture)
{
	return new IL_Sprite(position, scale, texture, IL_GetRenderer(), IL_GetDeltaTime());
}
IL_Sprite* inLine2D::IL_CreateSprite(Vector2f position, IL_Texture* texture)
{
	return new IL_Sprite(position, texture, IL_GetRenderer(), IL_GetDeltaTime());
}

void inLine2D::IL_DestroySprite(IL_Sprite* sprite)
{
	delete sprite;
}

IL_Text* inLine2D::IL_CreateText(TTF_Font* font, Color color, Vector2f position, char* contentText, unsigned int wrapWidth)
{
	return new IL_Text(font, color, position, IL_GetRenderer(), IL_GetDeltaTime(), contentText, wrapWidth);
}

void inLine2D::IL_DestroyText(IL_Text* text)
{
	delete text;
}

IL_SpriteList* inLine2D::IL_CreateSpriteList()
{
	return new IL_SpriteList();
}

void inLine2D::IL_DestroySpriteList(IL_SpriteList* spriteList)
{
	delete spriteList;
}

IL_Texture* inLine2D::IL_CreateTexture()
{
	return new IL_Texture(IL_GetRenderer());
}

IL_Texture* inLine2D::IL_CreateTexture(char* fileName)
{
	return new IL_Texture(fileName,IL_GetRenderer());
}

void inLine2D::IL_DestroyTexture(IL_Texture* texture)
{
	delete texture;
}

//IL_ParticleSystem* inLine2D::IL_CreateParticleSystem(Vector2i Position, float ParticleSize, float ParticleLifeTime, float ParticleSpawnRate, int ParticleAmount, IL_Texture* ParticleTexture)
//{
//	return new IL_ParticleSystem(Position, ParticleSize, ParticleLifeTime, ParticleSpawnRate, ParticleAmount, ParticleTexture, IL_GetRenderer(), IL_GetDeltaTime());
//}
//
//void inLine2D::IL_DestroyParticleSystem(IL_ParticleSystem* particleSystem)
//{
//	delete particleSystem;
//}

IL_Rectangle* inLine2D::IL_CreateRectangle(Vector2i position, Vector2i scale, Color color, Color color_border)
{
	return new IL_Rectangle(position, scale, color, color_border, IL_GetRenderer());
}
void inLine2D::IL_DestroyRectangle(IL_Rectangle* rectangle)
{
	delete rectangle;
}

IL_Timer* inLine2D::IL_CreateTimer(int seconds)
{
	return new IL_Timer(seconds);
}

void inLine2D::IL_DestroyTimer(IL_Timer* timer)
{
	delete timer;
}

IL_Button* inLine2D::IL_CreateButton(char* defaultButtonFilePath, char* pressedButtonFilePath, char* disabledButtonFilePath, TTF_Font* font, Color defaultTextColor, Color pressedTextColor, Color disabledTextColor, Vector2f position, Vector2f size, char* contentText)
{
	IL_Button* button = new IL_Button(defaultButtonFilePath, pressedButtonFilePath, disabledButtonFilePath, font, defaultTextColor, pressedTextColor, disabledTextColor, position, size, IL_GetRenderer(), IL_GetDeltaTime(), contentText);
	_GUIManager->AddObjectToListen(button);
	return button;
}

void inLine2D::IL_DestroyButton(IL_Button* button)
{
	delete button;
}

void iL2D::inLine2D::IL_TakeScreenShot(const char* savePath)
{
	SDL_Surface *screenshot = SDL_CreateRGBSurface(0, _screenInformation.width, _screenInformation.height, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
	SDL_RenderReadPixels(_Renderer, NULL, SDL_PIXELFORMAT_ARGB8888, screenshot->pixels, screenshot->pitch);
	SDL_SaveBMP(screenshot, savePath);
	SDL_FreeSurface(screenshot);
}
