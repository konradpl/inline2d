#include "Texture.h"


IL_Texture::IL_Texture(SDL_Renderer *renderer)
{
	_Renderer = renderer;
}

IL_Texture::IL_Texture(char* fileName, SDL_Renderer *renderer)
{
	_Renderer = renderer;
	LoadImage(fileName);
}

IL_Texture::~IL_Texture()
{
	SDL_DestroyTexture(_imageTexture);
	_imageTexture = NULL;
}

bool IL_Texture::LoadImage(char* fileName)
{
	bool success = true;

	_imageTexture = IMG_LoadTexture(_Renderer, fileName);

	if (_imageTexture == NULL)
	{
		Console::instance().Print("(inLine2D) - Unable to load image.\n *Error: ", SDL_GetError(), CONSOLE_ERROR);
		success = false;
	}
	else
		Console::instance().Print("(inLine2D) - Successfully loaded image:", fileName);

	return success;
}
