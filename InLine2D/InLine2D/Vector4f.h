#pragma once
#include "MultiPlatformDefines.h"
#include <SDL_rect.h>

class DLLEXPORT Vector4f
{
public:
	Vector4f();
	Vector4f(float x, float y, float w, float h);
	~Vector4f();
	SDL_Rect ToSDLRect();
	float x;
	float y;
	float w;
	float h;
};
