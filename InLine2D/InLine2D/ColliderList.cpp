#include "ColliderList.h"

IL_ColliderList::IL_ColliderList()
{
	*_follow.x = 0;
	*_follow.y = 0;
}


IL_ColliderList::IL_ColliderList(float *followX, float *followY)
{
	_follow.x = followX;
	_follow.y = followY;
}


IL_ColliderList::~IL_ColliderList()
{
	_colliderList.clear();
}

void IL_ColliderList::AddCollider(char* colliderName, Vector4f collider)
{
	if (_colliderList.find(colliderName) == _colliderList.end())
		_colliderList[colliderName] = collider;
	else
		Console::instance().Print("(InLine2D) - Failed at AddCollider, collider already exists: ", colliderName, CONSOLE_WARNING);
}

Vector4f IL_ColliderList::GetCollider(char* colliderName)
{
	if (_colliderList.find(colliderName) != _colliderList.end())
	{
		Vector4f colliderToReturn = _colliderList[colliderName];
		colliderToReturn.x += *_follow.x;
		colliderToReturn.y += *_follow.y;
		return colliderToReturn;
	}

	Console::instance().Print("(InLine2D) - Failed at GetCollider, collider does not exist: ", colliderName, CONSOLE_WARNING);
	return Vector4f(0, 0, 0, 0);
}


std::map<char*, Vector4f> IL_ColliderList::GetColliderList()
{
	return _colliderList;
}

void IL_ColliderList::RemoveCollider(char* colliderName)
{
	if (_colliderList.find(colliderName) != _colliderList.end())
	{
		_colliderList.erase(colliderName);
	}
	else
		Console::instance().Print("(InLine2D) - Failed at RemoveCollider, collider does not exist: ", colliderName, CONSOLE_WARNING);
}

void IL_ColliderList::ClearColliderList()
{
	_colliderList.clear();
}


bool IL_ColliderList::DetectCollision(char* colliderName, Vector4f collider)
{
	if (_colliderList.find(colliderName) != _colliderList.end())
	{
		Vector4f _colliderRect = (_colliderList)[colliderName];

		return !(*_follow.x +_colliderRect.x + _colliderRect.w < collider.x || *_follow.y + _colliderRect.y + _colliderRect.h < collider.y ||
			*_follow.x + _colliderRect.x > collider.x + collider.w || *_follow.y + _colliderRect.y > collider.y + collider.h);
	}
	else
		Console::instance().Print("(InLine2D) - Failed to DetectCollision, collider does not exist: ", colliderName, CONSOLE_WARNING);

	return false;
}

bool IL_ColliderList::DetectCollision(Vector4f collider)
{
	for (auto const &_colliderRect : _colliderList) 
	{
		if (!(*_follow.x + _colliderRect.second.x + _colliderRect.second.w < collider.x || *_follow.y +_colliderRect.second.y + _colliderRect.second.h < collider.y ||
			*_follow.x + _colliderRect.second.x > collider.x + collider.w || *_follow.y + _colliderRect.second.y > collider.y + collider.h))
			return true;
	}
	return false;
}
