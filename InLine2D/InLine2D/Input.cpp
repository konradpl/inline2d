#include "Input.h"

Input::Input(int *screen_width, int *screen_height)
{
	for (int i = 0; i < 322; i++) { // init them all to false
		KEYS_HELD_STATES[i] = NONE;
		KEYS_PRESSED_STATES[i] = NONE;
	}

	for (int i = 0; i < 5; i++) { // init them all to false
		MOUSE_PRESSED_STATES[i] = NONE;
		MOUSE_HELD_STATES[i] = NONE;
	}

	TOUCH_PRESSED_STATES = NONE;
	TOUCH_HELD_STATES = NONE;

	currentResolution.width = screen_width;
	currentResolution.height = screen_height;
}

Input::~Input(void)
{
}

bool Input::isMouseDown(int SDLK_MOUSE_ID)
{
	if (MOUSE_PRESSED_STATES[SDLK_MOUSE_ID] == PRESSED)
	{
		MOUSE_PRESSED_STATES[SDLK_MOUSE_ID] = HOLDING;
		return true;
	}
	else
		return false;
}

bool Input::isMouseHeld(int SDLK_MOUSE_ID)
{
	if (MOUSE_HELD_STATES[SDLK_MOUSE_ID] == HOLDING)
	{
		return true;
	}
	else
		return false;
}

bool Input::isMouseUp(int SDLK_MOUSE_ID)
{
	if (MOUSE_PRESSED_STATES[SDLK_MOUSE_ID] == RELEASED)
	{
		MOUSE_PRESSED_STATES[SDLK_MOUSE_ID] = NONE;
		return true;
	}
	else
		return false;
}

void Input::Update(SDL_Event* events)
{
	SDL_PumpEvents();
	const Uint8 *keystate = SDL_GetKeyboardState(NULL);
	Uint32 mousestate = SDL_GetMouseState(&_mousePosition.x, &_mousePosition.y);
					 
	if (events->type == SDL_MOUSEBUTTONDOWN)
	{
		if (mousestate)
		{
			if (MOUSE_PRESSED_STATES[events->button.button] != HOLDING)
				MOUSE_PRESSED_STATES[events->button.button] = PRESSED;

			MOUSE_HELD_STATES[events->button.button] = HOLDING;
		}
	}
	if (events->type == SDL_MOUSEBUTTONUP)
	{

			if (MOUSE_PRESSED_STATES[events->button.button] != RELEASED)
				MOUSE_PRESSED_STATES[events->button.button] = RELEASED;
			else
				MOUSE_PRESSED_STATES[events->button.button] = NONE;

			MOUSE_HELD_STATES[events->button.button] = NONE;
	}
	if (events->type == SDL_KEYDOWN)
	{
		if (keystate)
		{
			if (KEYS_PRESSED_STATES[events->key.keysym.sym] != HOLDING)
			KEYS_PRESSED_STATES[events->key.keysym.sym] = PRESSED;

			KEYS_HELD_STATES[events->key.keysym.sym] = HOLDING;
		}
		
	}
	if (events->type == SDL_KEYUP)
	{
		if (keystate)
		{
			if (KEYS_PRESSED_STATES[events->key.keysym.sym] != RELEASED)
				KEYS_PRESSED_STATES[events->key.keysym.sym] = RELEASED;
			else
				KEYS_PRESSED_STATES[events->key.keysym.sym] = NONE;

			KEYS_HELD_STATES[events->key.keysym.sym] = NONE;
		}
	}

	//touch down
	if (events->type == SDL_FINGERDOWN)
	{
		if (TOUCH_PRESSED_STATES != HOLDING)
			TOUCH_PRESSED_STATES = PRESSED;
		 
		TOUCH_HELD_STATES = HOLDING;

		_touchPosition.x = (int)(events->tfinger.x * *currentResolution.width);
		_touchPosition.y = (int)(events->tfinger.y * *currentResolution.height);
	}
	//Touch motion
	else if (events->type == SDL_FINGERMOTION)
	{
		TOUCH_HELD_STATES = HOLDING;

		_touchPosition.x = (int)(events->tfinger.x * *currentResolution.width);
		_touchPosition.y = (int)(events->tfinger.y * *currentResolution.height);
	}
	//Touch release
	else if (events->type == SDL_FINGERUP)
	{
		
		if (TOUCH_PRESSED_STATES != RELEASED)
			TOUCH_PRESSED_STATES = RELEASED;
		else
			TOUCH_PRESSED_STATES = NONE;

		TOUCH_HELD_STATES = NONE;

		_touchPosition.x = (int)(events->tfinger.x * *currentResolution.width);
		_touchPosition.y = (int)(events->tfinger.y * *currentResolution.height);
	}

}

bool Input::isKeyDown(int SDLK_ID)
{
	if (KEYS_PRESSED_STATES[SDLK_ID] == PRESSED)
	{
		KEYS_PRESSED_STATES[SDLK_ID] = HOLDING;
		return true;
	}
	else
		return false;
}

bool Input::isKeyHeld(int SDLK_ID)
{
	if (KEYS_HELD_STATES[SDLK_ID] == HOLDING)
	{
		return true;
	}
	else
		return false;
}

bool Input::isKeyUp(int SDLK_ID)
{
	if (KEYS_PRESSED_STATES[SDLK_ID] == RELEASED)
	{
		KEYS_PRESSED_STATES[SDLK_ID] = NONE;
		return true;
	}
	else
		return false;
}


bool Input::isTouchDown()
{
	if (TOUCH_PRESSED_STATES == PRESSED)
	{
		TOUCH_PRESSED_STATES = HOLDING;
		return true;
	}
	else
		return false;
}

bool Input::isTouchHeld()
{
	if (TOUCH_HELD_STATES == HOLDING)
	{
		return true;
	}
	else
		return false;
}

bool Input::isTouchUp()
{
	if (TOUCH_PRESSED_STATES == RELEASED)
	{
		TOUCH_PRESSED_STATES = NONE;
		return true;
	}
	else
		return false;
}