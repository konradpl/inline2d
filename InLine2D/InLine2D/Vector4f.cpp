#include "Vector4f.h"

Vector4f::Vector4f()
{
	x = 0;
	y = 0;
	w = 0;
	h = 0;
}

Vector4f::Vector4f(float x, float y, float w, float h)
{
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
}

Vector4f::~Vector4f()
{

}

SDL_Rect Vector4f::ToSDLRect()
{
	SDL_Rect rect;
	rect.x = (int)x;
	rect.y = (int)y;
	rect.w = (int)w;
	rect.h = (int)h;
	return rect;
}