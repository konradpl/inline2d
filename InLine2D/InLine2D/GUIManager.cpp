#include "GUIManager.h"

GUIManager::GUIManager()
{
	_objectListened = new std::vector<IL_Button*>;
	_mousePosition = Vector2i(0, 0);
}

GUIManager::~GUIManager()
{
	_objectListened->clear();
	delete _objectListened;
}

void GUIManager::AddObjectToListen(IL_Button* objToListen)
{
	_objectListened->push_back(objToListen);
}

void GUIManager::Update(SDL_Event* events)
{	  
	if (events->type == SDL_MOUSEBUTTONUP)
	{
		if (events->button.button == 1)
		{
			SDL_GetMouseState(&_mousePosition.x, &_mousePosition.y);

			for (std::vector<IL_Button*>::iterator it = _objectListened->begin(); it != _objectListened->end(); ++it)
			{
				if (!((*it)->GetX() + (*it)->GetW() / 2 < _mousePosition.x || (*it)->GetY() + (*it)->GetH() / 2 < _mousePosition.y ||
					(*it)->GetX() - (*it)->GetW() / 2 > _mousePosition.x || (*it)->GetY() - (*it)->GetH() / 2 > _mousePosition.y))
					(*it)->SetPressed(true);
				else
					(*it)->SetTextureUp();

			}
		}
	}
	if (events->type == SDL_MOUSEBUTTONDOWN)
	{
		if (events->button.button == 1)
		{
			SDL_GetMouseState(&_mousePosition.x, &_mousePosition.y);

			for (std::vector<IL_Button*>::iterator it = _objectListened->begin(); it != _objectListened->end(); ++it)
			{
				if (!((*it)->GetX() + (*it)->GetW() / 2 < _mousePosition.x || (*it)->GetY() + (*it)->GetH() / 2 < _mousePosition.y ||
					(*it)->GetX() - (*it)->GetW() / 2 > _mousePosition.x || (*it)->GetY() - (*it)->GetH() / 2 > _mousePosition.y))
					(*it)->SetTextureDown();
				else
					(*it)->SetTextureUp();
			}
		}
	}
}