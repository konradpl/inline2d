#pragma once
#include "stdafx.h"
#include "Console.h"

class DLLEXPORT IL_Texture
{
public:
	IL_Texture(SDL_Renderer *renderer);
	IL_Texture(char* fileName, SDL_Renderer *renderer);
	~IL_Texture();

	bool LoadImage(char* fileName);
	SDL_Texture* GetTexture() {
		return _imageTexture;
	}
private:
	SDL_Renderer* _Renderer;
	SDL_Texture* _imageTexture;

};

