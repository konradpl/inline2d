#pragma once
#include "MultiPlatformDefines.h"

class DLLEXPORT Vector3f
{
public:
	Vector3f();
	Vector3f(float x, float y, float z);
	~Vector3f();
	float x;
	float y;
	float z;
};
