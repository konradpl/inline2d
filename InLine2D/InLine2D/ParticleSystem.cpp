//#include "ParticleSystem.h"
//#include "Utility.h"
//
//
//IL_ParticleSystem::IL_ParticleSystem(Vector2i particlePosition, float particleSize, float particleLifeTime, float particleSpawnRate, int particleAmount, IL_Texture* texture, SDL_Renderer *renderer, float* deltaTime)
//{
//	_deltaTime = deltaTime;
//	_renderer = renderer;
//	_particlePosition = particlePosition;
//	_particleSize = particleSize;
//	_texture = texture;
//	_particleLifeTime = particleLifeTime;
//	_particleSpawnRate = particleSpawnRate;
//	_particleAmount = particleAmount;
//	_particleList = new std::vector<IL_Particle*>;
//	SetAlpha(1);
//	SetAreaOffset(Vector2i(0, 0), Vector2i(0, 0));
//}
//
//
//IL_ParticleSystem::~IL_ParticleSystem()
//{
//	for (std::vector <IL_Particle*>::iterator iterParticleList = _particleList->begin(); iterParticleList != _particleList->end(); ++iterParticleList)
//	{
//		delete (*iterParticleList);
//	}
//
//	_particleList->clear();
//
//	delete _particleList;
//}
//
//void IL_ParticleSystem::SetPosition(Vector2i position)
//{
//	_particlePosition = position;
//}
//
//void IL_ParticleSystem::SetVelocity(Vector2i velocity)
//{
//	_velocity.x = velocity.x;
//	_velocity.y = velocity.y;
//}
//
//void IL_ParticleSystem::SetAlpha(float alpha)
//{
//	_alpha = alpha * 255;
//
//}
//
//void IL_ParticleSystem::SetAngle(int angle)
//{
//	_angle = angle;
//}
//
//void IL_ParticleSystem::SetAreaOffset(Vector2i minOffset, Vector2i maxOffset)
//{
//	_minOffset.x = minOffset.x;
//	_minOffset.y = minOffset.y;
//	_maxOffset.x = maxOffset.x;
//	_maxOffset.y = maxOffset.y;
//}
//
//void IL_ParticleSystem::Draw()
//{
//	//if not max particles and x amount of time since last one created
//	if ((_particleList->size() < _particleAmount) && ((_timeCounter + _particleSpawnRate) <SDL_GetTicks()))
//	{
//		_timeCounter = SDL_GetTicks();
//	
//		
//		_particleList->push_back(new IL_Particle(_texture,  Vector2i(_particlePosition.x + Utility::instance().RandomRange(_minOffset.x, _maxOffset.x), _particlePosition.y + Utility::instance().RandomRange(_minOffset.y, _maxOffset.y)), _particleSize, _velocity, _angle, _alpha, SDL_GetTicks()));
//	}
//	
//	//draw/update loop of partilces
//	for (std::vector <IL_Particle*>::iterator iterParticleList = _particleList->begin(); iterParticleList != _particleList->end();)
//	{
//		(*iterParticleList)->Draw(_renderer, _deltaTime); //update/Draw Particle
//
//		//if time run out of particles Remove
//		if (SDL_GetTicks() - (*iterParticleList)->GetWhenCreated() > _particleLifeTime)
//		{
//			delete ((*iterParticleList));
//			iterParticleList = _particleList->erase(iterParticleList);
//		}
//		else
//			++iterParticleList; //move to other particle
//	}
//}
//
