#include "Button.h"

IL_Button::IL_Button(char* defaultButtonFilePath, char* pressedButtonFilePath, char* disabledButtonFilePath, TTF_Font* font, Color defaultColor, Color pressedColor, Color disabledColor, Vector2f position, Vector2f size, SDL_Renderer* renderer, float* deltaTime, char* contentText)
{
	_deltaTime = deltaTime;
	SetDeltaTime(_deltaTime);
	_pressed = false;
	_Renderer = renderer;
	_buttonTextColors.default = defaultColor;
	_buttonTextColors.pressed = pressedColor;
	_buttonTextColors.disabled = disabledColor;

	buttonText = new IL_Text(font, _buttonTextColors.default, position, _Renderer, _deltaTime, contentText);

	SetSize(size);
	SetPosition(position);

	_defaultButtonTexture = new IL_Texture(_Renderer);
	_defaultButtonTexture->LoadImageA(defaultButtonFilePath);

	_pressedButtonTexture = new IL_Texture(_Renderer);
	_pressedButtonTexture->LoadImageA(pressedButtonFilePath);

	_disabledButtonTexture = new IL_Texture(_Renderer);
	_disabledButtonTexture->LoadImageA(disabledButtonFilePath);

	_currentTexture = _defaultButtonTexture->GetTexture();
}


IL_Button::~IL_Button()
{	 
	delete buttonText;
	delete _defaultButtonTexture;
	delete _pressedButtonTexture;
	delete _disabledButtonTexture;

	_currentTexture = NULL;
}

void IL_Button::Rotate(double angle)
{
		_angle += angle * *_deltaTime;
		buttonText->Rotate(angle);
}

void IL_Button::Draw()
{
	SDL_RenderCopyEx(_Renderer, _currentTexture, NULL, &_imageRect.ToSDLRect(), _angle, &_imagePivot.ToSDLPoint(), _imageFlip);
	
	buttonText->Draw();
	//Draw
}

bool IL_Button::isPressed()
{
	if (_disabled) return false;

	if (_pressed) {
		_pressed = false;
		return true;
	}

	return false;
}

void IL_Button::SetPressed(bool toggle)
{
	if (!_disabled)
	{
		_currentTexture = _defaultButtonTexture->GetTexture();
		buttonText->SetColor(_buttonTextColors.default);
		_pressed = toggle;
	}
}


void IL_Button::SetDisable(bool toggle)
{
	_disabled = toggle;

	if (toggle)
		SetTextureDisable();
	else
		SetTextureUp();
}

void IL_Button::SetTextureUp()
{
	if (_disabled) return;
	_currentTexture = _defaultButtonTexture->GetTexture();
	buttonText->SetColor(_buttonTextColors.default);
}

void IL_Button::SetTextureDown()
{
	if (_disabled) return;

	_currentTexture = _pressedButtonTexture->GetTexture();
	buttonText->SetColor(_buttonTextColors.pressed);
}

void IL_Button::SetTextureDisable()
{
	_currentTexture = _disabledButtonTexture->GetTexture();
	buttonText->SetColor(_buttonTextColors.disabled);
}

void IL_Button::SetPosition(Vector2f position)
{
	_imageRect.x = position.x - _imagePivot.x;
	_imageRect.y = position.y - _imagePivot.y;

	buttonText->SetPosition(position);
}

IL_Text* IL_Button::GetTextData()
{
	return buttonText;
}