#pragma once
#include "stdafx.h"
#include <iostream>
class DLLEXPORT Input
{
public:
	Input(int *screen_width, int *screen_height);
	~Input(void);

	#define SDLK_MOUSELEFTCLICK 1
	#define SDLK_MOUSEMIDDLECLICK 2
	#define SDLK_MOUSERIGHTCLICK 3
	#define SDLK_MOUSEBACKCLICK 4
	#define SDLK_MOUSEFORWARDCLICK 5

	//returns true key was pressed
	bool isKeyDown(int SDLK_ID);
	//returns true key is being held
	bool isKeyHeld(int SDLK_ID);
	//returns true key was released
	bool isKeyUp(int SDLK_ID);
	//returns true if Mouse button was pressed
	bool isMouseDown(int SDLK_MOUSE_ID);
	//returns true if Mouse button is being held
	bool isMouseHeld(int SDLK_MOUSE_ID);
	//returns true if Mouse button was released
	bool isMouseUp(int SDLK_MOUSE_ID);

	//returns true if screen was touched
	bool isTouchDown();
	//returns true if screen is being touched
	bool isTouchHeld();
	//returns true if screen touch was released
	bool isTouchUp();

	//Do not use this function
	void Update(SDL_Event* events);

	//Get Mouse Position X
	int GetMouseX() { return _mousePosition.x; }
	//Get Mouse Position Y
	int GetMouseY() { return _mousePosition.y; }
	//Get Mouse Position in Vector2i
	Vector2i GetMousePosition() { return _mousePosition; }
	Vector2i GetTouchPosition() { return _touchPosition; }
private:
	//Mouse X and Y position
	Vector2i _mousePosition;
	//Touch X and Y position
	Vector2i _touchPosition;

	struct screen_resolution {
		int *width;
		int *height;
	};
	screen_resolution currentResolution;

	//possible key states
	enum KeyState
	{
		NONE = -1,
		PRESSED = 0,
		RELEASED = 1,
		HOLDING = 2
	};

	//Press and Release Mouse States
	int MOUSE_PRESSED_STATES[5]; //Up to 5 mouse buttons supported (left, middle, right, back, forward)
	//Held Mouse States
	int MOUSE_HELD_STATES[5];

	//Press and release state of touch
	int TOUCH_PRESSED_STATES;
	//held touch state
	int TOUCH_HELD_STATES;

	//Press and Release Key States
	int KEYS_HELD_STATES[322];  // 322 is the number of SDLK_DOWN events
	//Held Key States
	int KEYS_PRESSED_STATES[322];
};