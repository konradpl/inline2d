#pragma once
#include "stdafx.h"
#include "Texture.h"
#include "ColliderList.h"
#include "Console.h"
#include <map>

class DLLEXPORT IL_Sprite: public IL_Object
{
public:
	//set position and scale, pass in already existing texture to avoid loading
	IL_Sprite(Vector2f position, Vector2f scale, IL_Texture *texture, SDL_Renderer* renderer, float* deltaTime);
	//set position and image default scale, pass in already existing texture to avoid loading
	IL_Sprite(Vector2f position, IL_Texture *texture, SDL_Renderer* renderer, float* deltaTime);
	//set position and scale, loads teture file when created
	IL_Sprite(Vector2f position, Vector2f scale, char* fileName, SDL_Renderer* renderer, float* deltaTime);
	//set position and image default scale, loads texture file when created
	IL_Sprite(Vector2f position, char*  fileName, SDL_Renderer* renderer, float* deltaTime);
	~IL_Sprite();
	//Draw Sprite to Screen, (use OnDraw() function)
	void Draw();

	//animation setup
	void SetupSpriteSheet(int totalFramesX, int totalFramesY);
	//Sets to specific frame in animation (doesnt need to be updated per frame)
	void SetAnimationFrame(int xFrame, int yFrame);
	//adding animation to animation list
	void AddAnimation(char*  animName, int startFrameX, int endFrameX, int yFrame);
	//remove animation from animation list
	void RemoveAnimation(char* animName);
	//get current animation frame
	int GetCurrentAnimationFrame();
	//set current animation frame
	void SetCurrentAnimationFrame(int frame);
	//Playing specific animation which was Addded ( use in OnUpdate() )
	void PlayAnimation(char*  animName, float animSpeed);

	//get distance between given position and position of this sprite
	double GetDistance(Vector2i positionB);
	//add collider and give it a name. The position of collider will be at center of sprite. The size of collider will be the size of sprite 
	void AddCollider(char* colliderName);
	//add collider and give it a name. Additionally set the size of collider, the collider is automatically centered to sprite
	void AddCollider(char* colliderName, float w, float h);
	//add colllider and give it a name. Set offset position of collider (from center of collider). Set size of the collider.
	void AddCollider(char* colliderName, float x, float y, float w, float h);  
	//remove collider collider from the list with specific name
	void RemoveCollider(char* colliderName);
	//Get collider with specific name
	Vector4f GetCollider(char* colliderName);
	//detect collision with specific collider
	bool DetectCollision(char* colliderName, Vector4f collider);
	//detect collision with all colliders
	bool DetectCollision(Vector4f collider);
	//draw all colliders in this sprite
	void SetDrawColliders(bool toggle);
	//get if it is drawing the colliders
	bool GetDrawColliders();
private:
	void Initialize(SDL_Renderer* renderer, float *deltaTime);
	SDL_Renderer* _Renderer = NULL;

	bool _deleteTextureLoader;
	IL_Texture *_textureLoader;

	SDL_Rect _sourceRect;

	IL_ColliderList *_colliderList;
	bool _drawColliders;

	struct Animation{ int startFrameX; int endFrameX; int yFrame; };
	std::map<char*, Animation> *_animationlist;
	int _currentAnimFrame;
	int _animTimeTracking;
	Vector2i _Frames;
};

