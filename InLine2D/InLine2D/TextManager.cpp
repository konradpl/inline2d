#include "TextManager.h"


TextManager::TextManager()
{
	_fontList = new std::map <char*, TTF_Font*>;
	if (!TTF_WasInit() && TTF_Init() == -1) {
		Console::instance().Print("(inLine2D) - Couldn't initialize TTF (Text).\n *Error: ", TTF_GetError(), CONSOLE_ERROR);
	}
}


TextManager::~TextManager()
{
	for (auto const &ent1 : *_fontList) {
		TTF_CloseFont(ent1.second);
	}
	_fontList->clear();

	TTF_Quit();
}

TTF_Font* TextManager::GetFont(char* fontIDname)
{
	if ((*_fontList)[fontIDname] != NULL)
	return (*_fontList)[fontIDname];

	Console::instance().Print("(inLine2D) - fontIDname does not exist: ", fontIDname, CONSOLE_WARNING);
	return NULL;
}

void TextManager::AddFont(char* filePath, char* fontIDname, int fontSize)
{
	for (auto const &ent1 : *_fontList) {
		if (ent1.first == fontIDname)
		{
			Console::instance().Print("(inLine2D) - Could not add font, because fontIDname already exists: ", fontIDname, CONSOLE_WARNING);
			return;
		}
	}

	TTF_Font *font;
	font = TTF_OpenFont(filePath, fontSize);
	if (!font)
	{
		Console::instance().Print("(inLine2D) - Couldn't open font file.\n *Error: ", TTF_GetError(), CONSOLE_ERROR);
	}
	(*_fontList)[fontIDname] = font;
}

void TextManager::RemoveFont(char * fontIDname)
{
	if ((*_fontList)[fontIDname] != NULL)
	{
		TTF_CloseFont((*_fontList)[fontIDname]);
		(*_fontList).erase(fontIDname);
	}
	else
		Console::instance().Print("(inLine2D) - Font could not be removed, font name doesn't exist: ", fontIDname, CONSOLE_WARNING);
}
