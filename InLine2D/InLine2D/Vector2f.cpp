#include "Vector2f.h"

Vector2f::Vector2f()
{
	x = 0;
	y = 0;
}

Vector2f::Vector2f(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vector2f::~Vector2f()
{

}

SDL_Point Vector2f::ToSDLPoint()
{
	SDL_Point point;
	point.x = (int)x;
	point.y = (int)y;
	return point;
}