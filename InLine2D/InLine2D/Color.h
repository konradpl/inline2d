#pragma once
#include "MultiPlatformDefines.h"

class DLLEXPORT Color
{
public:
	Color();
	Color(float r, float g, float b, float a);
	Color(float r, float g, float b);
	~Color();
	float red;
	float green;
	float blue;
	float alpha;
};
