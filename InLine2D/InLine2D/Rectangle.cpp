#include "Rectangle.h"


IL_Rectangle::IL_Rectangle(Vector2i position, Vector2i size, Color color, Color color_border, SDL_Renderer* renderer)
{
	_Renderer = renderer;
	SetPosition(position);
	SetSize(size);
	SetColor(color);
	SetColorBorder(color_border);
}


IL_Rectangle::~IL_Rectangle()
{
}

void IL_Rectangle::SetColor(Color color)
{
	_color = color;
}

void IL_Rectangle::SetColorBorder(Color color_border)
{
	_color_border = color_border;
}


void IL_Rectangle::SetPosition(Vector2i position)
{
	_rect.x = position.x;
	_rect.y = position.y;
}

void IL_Rectangle::SetSize(Vector2i size)
{
	_rect.w = size.x; 
	_rect.h = size.y;
}

void IL_Rectangle::Draw()
{
	SDL_SetRenderDrawColor(_Renderer, (Uint8)_color.red, (Uint8)_color.green, (Uint8)_color.blue, 255);
	SDL_RenderFillRect(_Renderer, &_rect);
	SDL_SetRenderDrawColor(_Renderer, (Uint8)_color_border.red, (Uint8)_color_border.green, (Uint8)_color_border.blue, 255);
	SDL_RenderDrawRect(_Renderer, &_rect);
}
