#include "SpriteList.h"

IL_SpriteList::IL_SpriteList()
{
	_spriteList = new std::vector< IL_Sprite*>;
}

IL_SpriteList::~IL_SpriteList()
{
	ClearSpriteList();
	delete _spriteList;
}

void IL_SpriteList::AddSprite(IL_Sprite* newSprite)
{
	_spriteList->push_back(newSprite);
}

void IL_SpriteList::RemoveSprite(int index)
{

	_spriteList->erase(_spriteList->begin() + index);

}

void IL_SpriteList::DrawAll()
{
	for (std::vector <IL_Sprite*>::iterator iterSpriteList = _spriteList->begin(); iterSpriteList != _spriteList->end(); iterSpriteList++)
	{
		(*iterSpriteList)->Draw();
	}
}

void IL_SpriteList::DrawSprite(int index)
{
		(*_spriteList)[index]->Draw();
}

IL_Sprite* IL_SpriteList::GetSprite(int index)
{
	return (*_spriteList)[index];
}

int IL_SpriteList::GetListSize()
{
	return (*_spriteList).size();
}

void IL_SpriteList::ClearSpriteList()
{

	for (std::vector <IL_Sprite*>::iterator iterSpriteList = _spriteList->begin(); iterSpriteList != _spriteList->end(); iterSpriteList++)
	{
		delete (*iterSpriteList);
	}
	
	_spriteList->clear();
}